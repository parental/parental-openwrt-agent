#!/bin/sh
echo "Starting Parental Control Installer"
echo "This script will install the Parental Control Agent on your router."
echo ""
echo -n "Please enter the id of the router: "
read parental_id
echo -n "Please enter the password of the router: "
read parental_key

echo ""
echo ""

echo "Installing dependecies"
opkg update > /dev/null

echo ""
echo "Installing socat"
opkg install socat > /dev/null

echo ""
echo "Installing inotify-tools"
opkg install inotifywait > /dev/null

echo ""
echo "Installing openvpn"
opkg install openvpn-openssl > /dev/null

echo ""
echo "Installing curl"
opkg install curl > /dev/null

echo ""
echo "Installing nohup"
opkg install coreutils-nohup > /dev/null

echo ""
echo "Installing killall"
opkg install coreutils-kill > /dev/null

echo ""
echo "Installing sleep"
opkg install coreutils-sleep > /dev/null

echo ""
echo "Installing xargs"
opkg install findutils-xargs > /dev/null

echo ""
echo "Installing base64"
opkg install coreutils-base64 > /dev/null

echo ""
echo "Installing conntrack"
opkg install conntrack > /dev/null

echo ""
echo ""
echo "Downloading Parental Control Agent"
curl -s -o /tmp/parental_control.tar https://gitlab.com/parental/parental-openwrt-agent/-/archive/main/parental-openwrt-agent-main.tar

echo ""
echo "Extracting Parental Control Agent"
rm -rf /root/parental-*
tar -xvf /tmp/parental_control.tar -C /tmp/

echo ""
echo "Installing Parental Control Agent"
cp -r /tmp/parental*/* /

echo $parental_id > /root/parental/id
echo $parental_key > /root/parental/key

chmod +x /root/parental/postinstall
/root/parental/postinstall

echo ""
echo ""
echo ""
echo "Parental Control Agent installed successfully"
echo "Going to reboot now"
echo ""
echo "Press any key to reboot"
read

reboot
