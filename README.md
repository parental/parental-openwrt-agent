# Agente de OpenWRT
## Comandos para instalar
Para instalarlo hay que conectarse por SSH y ejecutar el instalador de la siguiente manera:

```bsh
opkg update
opkg install curl
curl -s https://gitlab.com/parental/parental-openwrt-agent/-/raw/main/install.sh > /tmp/install.sh
chmod +x /tmp/install.sh
/tmp/install.sh
```